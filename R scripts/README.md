# Release letter Risk Analysis 1.3

*   Countries: The Netherlands
*   Client name: Risk Analysis Automation
*   Date release: 2020-05-20
*   Business-line: Credit Management
*   Git repository: [https://bitbucket.graydon-global.com/bitbucket/projects/ANAT/repos/test_risk_analysis_nl/browse]

## 0. !!! Always IMPORTANT !!!
Every time before running the project, make sure you pull the latest version and use the main branch.


## 1. Additions after latest version

This report is forked from ta_0450_ew_cleaning. But actually it is a demo for future automation of risk analysis, so we make following changes to make it happen:
* config file standardized. The config file has a clearer structure corresponding the document structure. This config file can be used for other projects too, if some parameters are changed as per the project requests.
* new scripts to generate market data in one time for all projects and make reports for different clients in one time
* load_xseption, load_all and report.rmd are changed because of the new structure of files
* include cov-19 xseption and cov-19 impact score related plots
* make sure the xseption table only include no more than 10 obs for plotting
* exclude missing lon and lat in the map
* delete duplicates in red-flag and orange-flag tables
* change some data processing into different functions in the report
* directory changes based on the new structure of files



## 2. !!!PENDING IMPROVEMENT!!!
* potential redoing everything after we get the new source from GDI
* weird characters caused by encoding...
* include a cov-19 module on-off parameter in config?





# Following are not updated
    
## 4. Configuration

Purpose: Scripting variables that are expected to make the script run client specific
The configuration files should contain enough inline documentation.

*   File name: config.yml


## 5. Output scripts

#### Reports

The Rmd file generating overall report

*   File name: report.RMD
*   Objective: Create report :) 
*   Output: HTML page




!!!!!!!!!!!!!!!!!
!!! IMPORTANT !!!
!!!!!!!!!!!!!!!!!

#### Miscellaneous

1)

NEW!

*  From now on, only the config-file should be changed for a new customer. (at least in theory :) ) 

* This analysis is still run of mainly SAS data. But the customer-file is enriched, so we do use some data from Insights. 
    (e.g.: # employees)



## 6. Data processing

Scripts that are used for data loading and transformation and integration of data sources.

#### Integration script(s):

*   Script file: load_all.R
*   Purpose:
    *   data prep
    *   determine benchmark (focus-branche and focus-awp based on graydon_id of specific supplier)
    *   joining market, xseptions and financial data
*   Raw input file(s):
    *   load_market_nl.R or input _r_market_nl.RDS
    *   load_xseptions.R or input_r_xseptions_nl.RDS
    *   load_financials.R or input_r_financials_nl.RDS

#### Data source script(s):

##### Load market data

*   Script file: load_market_nl.R
*   Raw input file(s):
    *   S:/Analytics/General analytics/Base tables/market_nl/20191003/market_all_scores.csv
    
|                                                                                                                              |     |                                                                                                 | 
|------------------------------------------------------------------------------------------------------------------------------|-----|-------------------------------------------------------------------------------------------------| 
| id_bedrijfsnummer                                                                                                            | chr | "3", "4", "5", "6", "9", "11", "12", "14", "16", "17", "18", "19", "22", "23", "25", "26", "... | 
| code_legal_form                                                                                                              | chr | "Besloten vennootschap", "Besloten vennootschap", "Besloten vennootschap", "Besloten vennoot... | 
| date_start                                                                                                                   | chr | "17MAR1921", "01JAN1920", "01JAN1740", "20NOV1874", "01MAY1897", "", "01AUG1924", "01MAY1821... | 
| is_stopped                                                                                                                   | int | 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1,... | 
| date_stopped																												   | chr | "", "30DEC2006", "", "", "", "23DEC1983", "", "", "20NOV1999", "", "16APR2007", "01JAN2011",... |
| postal_6                                                                                                                     | chr | "7556BE", "6041XB", "6211SW", "1508GE", "9401EL", "4612PJ", "4385BB", "1544BL", "6828AM", "6... | 
| code_sbi                                                                                                                     | chr | "6420", "0002", "4634", "4120", "47191", "", "6420", "6420", "47644", "6420", "2530", "0002"... | 
| rating_pd                                                                                                                    | chr | "CCC", "NR", "BBB", "D", "B", "D", "A", "BBB", "NR", "A", "NR", "NR", "NR", "D", "D", "NR", ... | 
| amt_credit_limit                                                                                                             | dbl | 140000, 0, 200000, 0, 550000, 0, 70000, 60000, 0, 70000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,... | 
| perc_pd                                                                                                                      | dbl | 1.50, NA, 0.22, 100.00, 1.19, 100.00, 0.15, 0.19, NA, 0.13, NA, NA, NA, 100.00, 100.00, NA, ... | 
| code_credit_flag                                                                                                             | chr | "O", "R", "G", "R", "G", "R", "G", "G", "R", "G", "R", "R", "R", "R", "R", "R", "R", "R", "R... | 
| perc_discontinuation                                                                                                         | dbl | 1.07, NA, 0.14, NA, 0.14, NA, 4.00, 10.13, NA, 1.13, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,... | 
| rating_discontinuation                                                                                                       | dbl | 7, NA, 8, NA, 8, NA, 4, 3, NA, 6, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, 5, NA, NA,... | 
| score_growth                                                                                                                 | dbl | 99, NA, 2, NA, 3, NA, 99, 3, NA, 3, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, 2, NA, N... | 
| date_scores                                                                                                                  | chr | "27MAY19", "27MAY19", "27MAY19", "27MAY19", "27MAY19", "27MAY19", "27MAY19", "27MAY19", "27M... | 
| type_company                                                                                                                 | chr | "S", "M", "M", "S", "S", "M", "M", "M", "M", "M", "M", "M", "M", "M", "M", "M", "M", "M", "M... | 
| number_employees                                                                                                             | dbl | 1, 0, 9, 0, 45, 0, 5, 6, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0... | 
| id_mothercompany                                                                                                             | chr | "894394029", "", "1064993", "994026", "901364681", "", "", "893025526", "", "", "", "", "", ... | 
| score_payment_behaviour                                                                                                      | dbl | 6.6, NA, 8.5, 1.0, 8.5, NA, 7.0, 7.0, NA, 7.0, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N... |
| id_aansprakelijk_bedrijf																									   | chr |	"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ... |

	*   S:/Analytics/General analytics/Base tables/market_nl/20191003/list_all_xseptions.csv

|                                                                                                                                         |     |                                                                                                                | 
|-----------------------------------------------------------------------------------------------------------------------------------------|-----|----------------------------------------------------------------------------------------------------------------| 
| BEDRIJFSNUMMER                                                                                                                          | chr | "6", "11", "23", "25", "29", "41", "76", "86", "91", "103", "112", "119", "131", "132", "138", "153", "165"... | 
| XSEPTIONNUMMER                                                                                                                          | chr | "79", "79", "79", "79", "79", "80", "79", "79", "79", "79", "79", "79", "79", "79", "79", "79", "80", "79",... | 
| STARTDATUM                                                                                                                              | chr | "29JAN2013", "10APR1981", "14AUG1991", "06NOV1997", "19APR1985", "09FEB1983", "02JUL1985", "07NOV1990", "16... | 
| STARTDATUMREGISTRATIE                                                                                                                   | chr | "30JAN2013", "21JUN2012", "21JUN2012", "21JUN2012", "21JUN2012", "21JUN2012", "21JUN2012", "21JUN2012", "21... | 
| EXPIRATIEDATUM                                                                                                                          | chr | "21SEP2026", "23DEC1991", "10APR2004", "17JUN2007", "30JAN1995", "", "01MAY1998", "15JUL2000", "19NOV2005",... | 
| EINDDATUM																																  | chr	| "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",... |          | 
| EINDDATUMREGISTRATIE																													  | chr | "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",... | 

	*   S:/Analytics/JIRA - BAU/TA-0379_Frisia/render_all_config.csv

|               |     |                                                                                                                                   | 
|---------------|-----|-----------------------------------------------------------------------------------------------------------------------------------| 
| name_supplier | chr | "Adriaan van Erk Timmerfabriek B.V.", "AKN Afwerkvloeren B.V.", "Aannemingsbedrijf Project Rotterdam B.V.", "Barendrecht Elekt... | 
| id_supplier   | chr | "229682", "995078", "903650819", "284055", "80727", "896646963", "909584532", "901665878", "930890302", "904918866", "548892",... | 
| id_company    | chr | "116131", "806622", "2856637", "30843", "2560830", "2932506", "1816806", "1290512", "4568913", "1618138", "314007", "454052", ... | 

	*   S:/Analytics/JIRA - BAU/TA-0379_Frisia/enriched_companies.csv

|                                            |     |                                                                                           | 
|--------------------------------------------|-----|-------------------------------------------------------------------------------------------| 
| id_graydon                                 | chr | "5", "6", "98", "107", "117", "171", "182", "232", "253", "303", "356", "408", "453", ... | 
| Omschrijving_Aantal_werkz_pers_klasse_code | chr | "10 tot 19", "50 tot 99", "20 tot 49", "2 tot 4", "2 tot 4", "", "5 tot 9", "2 tot 4",... | 
| KVK_nummer_8                               | chr | "14600001", "35000001", "12000019", "22000021", "8000023", "", "21000035", "2000045", ... | 
| Organisatienaam                            | chr | "G.Thiessen Wijnkopers Sedert 1740 B.V.", "Bouwbedrijf Kakes B.V.", "Collin B.V.", "Fi... | 


##### Load XSeptions data

*   Script file: load_xseptions.R
*   Raw input file(s):
    *   market_all_xseptions.csv

|                                                                                                                                      |     |                                                                                                        | 
|--------------------------------------------------------------------------------------------------------------------------------------|-----|--------------------------------------------------------------------------------------------------------| 
| id_bedrijfsnummer                                                                                                                    | chr | "3", "3", "4", "4", "6", "6", "9", "11", "16", "18", "19", "19", "22", "23", "25", "26", "28", "28"... | 
| code_xseption                                                                                                                        | chr | "21", "26", "19", "75", "18", "79", "28", "77", "75", "75", "19", "75", "75", "77", "77", "73", "26... | 
| description_xseption                                                                                                                 | chr | "Een deel van de activiteiten van dit bedrijf voortgezet in een ander bedrijf.", "Dit bedrijf zet d... | 
| code_xseption_category                                                                                                               | chr | "BA", "BA", "BA", "BA", "BA", "F", "BA", "BA", "BA", "BA", "BA", "BA", "BA", "BA", "BA", "BA", "BA"... | 
| description_xseption_category                                                                                                        | chr | "Bedrijfsactiviteiten", "Bedrijfsactiviteiten", "Bedrijfsactiviteiten", "Bedrijfsactiviteiten", "Be... | 
| date_start_xseption                                                                                                                  | chr | "31MAY2013", "19MAY2015", "30DEC2006", "30DEC2006", "01FEB2013", "29JAN2013", "01OCT2009", "23DEC19... | 
| date_end_xseption																													   | chr | "", "", "", "", "", "21SEP2026", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""... | 
| `days since end xseption`																											   | chr | "", "", "", "", "", "-2674", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""... | 
| id_bedrijfsnummer_2                                                                                                                  | chr | "928711528", "895660016", "898109906", "0", "23781", "0", "911141383", "0", "0", "0", "926270338", ... | 


##### Load Financial data

*   Script file: load_financials.R
*   Raw input file(s):
    *   market_nl_financials.csv

|                     |     |                                                                                                                  | 
|---------------------|-----|------------------------------------------------------------------------------------------------------------------| 
| id_graydon          | chr | "3", "3", "3", "3", "3", "3", "3", "5", "6", "6", "9", "9", "9", "9", "9", "9", "9", "12", "12", "12", "12", ... | 
| year_financials     | int | 2017, 2015, 2014, 2013, 2012, 2011, 2010, 2017, 2011, 2010, 2017, 2016, 2015, 2014, 2013, 2012, 2010, 2018, 2... | 
| amt_total_assets    | dbl | 21693000, 73099000, 78413000, 82172000, 80441000, 87326000, 82132000, 921811, 4162421, 6072309, 13399291, 132... | 
| amt_current_ratio   | dbl | 0.87, 1.04, 1.13, 1.06, 1.06, 1.07, 1.43, 2.79, 0.49, 1.47, 1.01, 1.02, 1.04, 0.82, 1.02, 0.81, 3.83, 53.06, ... | 
| amt_solvancy_ratio  | dbl | 31.36, 38.31, 38.34, 36.81, 34.25, 34.11, 45.45, 74.04, 16.80, 32.45, 57.37, 57.17, 55.75, 54.35, 55.64, 55.3... | 
| amt_equity          | dbl | 6803000, 28006000, 30069000, 30255000, 27556000, 29790000, 37330000, 682600, 699456, 1970933, 7687645, 758117... | 
| amt_working_capital | dbl | -1647000, 1592000, 4317000, 2550000, 2328000, 2982000, 12287000, 428319, -1644090, 1905154, 26588, 42364, 779... | 
| code_annual_account | chr | "vennootschappelijk", "geconsolideerd", "geconsolideerd", "geconsolideerd", "geconsolideerd", "geconsolideerd... | 

##### Extra financials

