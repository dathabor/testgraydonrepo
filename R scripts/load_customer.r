prep_data_customer <- function(config) {
  
  if(config$process_customer){
    
    # 1. Load data ---- 
    #### Customer data
    tbl_customer <- fread(paste0(config$dir_customer, "/",config$file_customer),
                               colClasses = "character",
                               data.table = FALSE,
                               encoding = "UTF-8") 
    
    ### GDI matching table1 (tbl_market_branches) - at this moment we use this table because it is most updated
    tbl_market_branches <- fread(paste0(config$dir_input, "/", config$file_market_branches), data.table = FALSE, encoding = "UTF-8") %>%
      select(id_company = GDB_ORG_YUID,
             id_graydon = NL_Y_COMPANY_ID,
             id_kvk_12 = NL_KVK_NUMBER_12,
             id_kvk_branche = NL_KVK_BRANCH_NUMBER,
             name_company = PRINCIPAL_NAME) %>%
      mutate(id_company = as.character(id_company),
             id_graydon = as.character(id_graydon))
    
    # #### GDI matching table2 (tbl_id_mapping)
    # tbl_id_matching <- fread(paste0(config$dir_input, "/", config$file_id_matching), data.table = FALSE, encoding = "UTF-8") %>%
    #   select(id_company = ORG_GDB_ORG_YUID,
    #          id_graydon = ID_VALUE) %>%
    #   mutate(id_company = as.character(id_company),
    #          id_graydon = as.character(id_graydon)) 
    
    # 2. Transform data ----
    ### create and select vars
    tbl_customer %<>%
      mutate(number_employees_new = as.numeric(word(Omschrijving_Aantal_werkz_pers_klasse_code, start=1)),
             is_customer = TRUE) %>% 
      select(id_customer_number = config$id_customer_number,
             id_customer_name_company = config$id_customer_name_company,
             id_customer_kvk = config$id_customer_kvk,
             id_kvk = KVK_nummer_8,
             id_company = Graydon_nummer,
             organisatienaam = Organisatienaam,
             number_employees_new,
             is_customer)
    
    ## add id_graydon from matching table1
    tbl_customer %<>%
      left_join(select(tbl_market_branches, id_company, id_graydon), by = "id_company")
    
    # ### add id_graydon from matching table2
    # tbl_customer %<>%
    #   left_join(select(tbl_id_matching, id_company, id_graydon), by = "id_company") %>% 
    #   mutate(id_graydon = ifelse(id_company == "44634087", "890314489", id_graydon)) # fix bugs
    # rm(tbl_id_matching)
    
    # 3. Write RDS file ----
    saveRDS(object = tbl_customer, file = paste0(config$dir_intermediate, "/input_r_customer.RDS"))
    
  } else {
    tbl_customer <- readRDS(paste0(config$dir_intermediate, "/input_r_customer.RDS"))
  }
  
  if (nrow(tbl_customer) == 0) {
    stop("No data in tbl_customer")
  } 
  return(tbl_customer)
}  